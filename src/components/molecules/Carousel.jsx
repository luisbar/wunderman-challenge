import React from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import Product from '@molecules/Product';
import { useResizeListenerContext } from '@atoms/ResizeListenerProvider';
import propTypes from 'prop-types';
import Arrow from '@atoms/Arrow';

const firstProductImage = require('@images/first-product.svg').default;
const firstProductMobileImage = require('@images/first-product-mobile.png').default;
const secondProductImage = require('@images/second-product.svg').default;
const secondProductMobileImage = require('@images/second-product-mobile.png').default;

const Carousel = ({ button }) => {
  const { size } = useResizeListenerContext();
  const firstProduct = size === 'sm' || size === 'md' ? firstProductMobileImage : firstProductImage;
  const secondProduct = size === 'sm' || size === 'md' ? secondProductMobileImage : secondProductImage;
  const settings = {
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    nextArrow: <Arrow direction='right'/>,
    prevArrow: <Arrow direction='left'/>,
  };

  return (
    <Slider
      {...settings}
    >
      <Product
        button={button}
        image={firstProduct}
      />
      <Product
        button={button}
        image={secondProduct}
      />
    </Slider>
  );
};

Carousel.propTypes = {
  button: propTypes.shape(Product.propTypes.button).isRequired,
};

export default Carousel;
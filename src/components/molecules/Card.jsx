import React from 'react';
import Text from '@atoms/Text';
import Image from '@atoms/Image';
import propTypes from 'prop-types';

const Card = ({ title, body, image, className }) => (
  <div
    className={`
    rounded-md shadow-md hover:shadow-xl dark:bg-dark-200 overflow-hidden ${className}
    `}
  >
    <Image
      className='w-full object-cover h-1/2'
      src={image}
    />
    <Text
      as='h4'
      label={title}
      className='m-4'
    />
    <Text
      as='md'
      label={body}
      className='m-4'
    />
  </div>
);

Card.propTypes = {
  title: propTypes.string.isRequired,
  body: propTypes.string.isRequired,
  image: propTypes.string.isRequired,
  className: propTypes.string,
};

Card.defaultProps = {
  className: '',
};

export default Card;
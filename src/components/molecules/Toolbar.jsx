import React from 'react';
import Dropdown from '@atoms/Dropdown';
import Text from '@atoms/Text';
import { useStoreContext } from '@stateMachines/Store';
import { useInternationalizationContext } from '@internationalization/InternationalizationProvider';
const languages = {
  Español: 'es',
  Spanish: 'es',
  Ingles: 'en',
  English: 'en',
};

const Toolbar = () => {
  const { homeService } = useStoreContext();
  const setLanguage = [ ...useInternationalizationContext() ].pop();

  const onChangeLanguage = ({ target }) => {
    setLanguage(languages[target.value]);
  };

  return (
    <header
      className='w-full h-16 bg-dark-100 flex justify-between items-center'
    >
      <Text
        as='h5'
        className='text-light-100 ml-6'
        label='toolbar.txt4'
      />
      <Dropdown
        className='mb-6 mr-6'
        machine={homeService.children.get('languagesDropdown')}
        select={{
          options: [
            {
              key: 'en',
              value: 'toolbar.txt1',
            },
            {
              key: 'es',
              value: 'toolbar.txt2',
            },
          ],
          defaultValue: 'toolbar.txt1',
          onChange: onChangeLanguage,
        }}
        text={{
          label: 'toolbar.txt3',
        }}
      />
    </header>
  );
};

export default Toolbar;
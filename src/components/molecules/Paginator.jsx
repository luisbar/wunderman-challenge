import propTypes from 'prop-types';
import React from 'react';
import Button from '@atoms/Button';
import { useActor } from '@xstate/react';

const Paginator = ({ machine, className }) => {
  const [ state ] = useActor(machine);

  return (
    <div
      className={`flex flex-row justify-end ${className}`}
    >
      <Button
        machine={state.children.previousButton}
        className='row-start-1 col-start-1 bg-light-100 border-accent-100 border mr-5'
        text={{
          label: 'paginator.txt1',
          className: 'text-accent-100 dark:text-accent-100',
        }}
        data={{
          type: 'PREVIOUS',
        }}
      />
      <Button
        machine={state.children.nextButton}
        className='row-start-1 col-start-2 bg-light-100 border-accent-100 border'
        text={{
          label: 'paginator.txt2',
          className: 'text-accent-100 dark:text-accent-100',
        }}
        data={{
          type: 'NEXT',
        }}
      />
    </div>
  );
};

Paginator.propTypes = {
  machine: propTypes.object.isRequired,
  className: propTypes.string.isRequired,
};

export default Paginator;
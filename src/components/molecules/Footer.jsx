import React from 'react';

const Footer = () => (
  <footer
    className='w-full h-16 bg-light-100'
  />
);

export default Footer;
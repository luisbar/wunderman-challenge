import React from 'react';
import Button from '@atoms/Button';
import Image from '@atoms/Image';
import propTypes from 'prop-types';

const Product = ({ button, image }) => (
  <div>
    <div
      className='grid grid-rows-splitted-into-10 grid-cols-splitted-into-10'
    >
      <Image
        className='row-start-1 row-span-10 col-start-1 col-span-10 w-full object-cover'
        src={image}
      />
      <Button
        className='row-start-7 row-span-1 col-start-2 col-span-3 md:row-start-6 md:row-span-2 md:col-start-2 md:col-span-2 xl:row-start-6 xl:row-span-1'
        machine={button.machine}
        text={button.text}
        data={button.data}
      />
    </div>
  </div>
);

Product.propTypes = {
  button: propTypes.shape(Button.propTypes).isRequired,
  image: propTypes.string.isRequired,
};

export default Product;
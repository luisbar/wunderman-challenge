import propTypes from 'prop-types';
import React from 'react';
import Toolbar from '@molecules/Toolbar';
import Footer from '@molecules/Footer';

const Main = ({ children }) => (
  <main
    className='h-auto flex flex-col dark:bg-dark-100'
  >
    <Toolbar />
    {children}
    <Footer />
  </main>
);

Main.propTypes = {
  children: propTypes.node.isRequired,
};

export default Main;
import propTypes from 'prop-types';
import React from 'react';

const Centered = ({ children, className }) => (
  <div className={`mx-auto flex flex-col justify-around items-center bg-primary ${className}`}>
    {children}
  </div>
);

Centered.propTypes = {
  children: propTypes.node.isRequired,
  className: propTypes.string,
};

Centered.defaultProps = {
  className: '',
};

export default Centered;
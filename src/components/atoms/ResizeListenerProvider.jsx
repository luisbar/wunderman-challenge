import React from 'react';
import propTypes from 'prop-types';

const sizes = {
  sm: 640,
  md: 768,
  lg: 1024,
  xl: 1280,
  '2xl': 1536,
};

const getSize = () => {
  if (window.innerWidth <= sizes.sm) return 'sm';
  if (window.innerWidth <= sizes.md) return 'md';
  if (window.innerWidth <= sizes.lg) return 'lg';
  if (window.innerWidth <= sizes.xl) return 'xl';

  return '2xl';
};

export const ResizeListenerContext = React.createContext();

const ResizeListenerProvider = ({ children }) => {
  const [ size, setSize ] = React.useState(getSize());

  const handleResize = () => {
    setSize(getSize());
  };

  React.useEffect(() => {
    window.addEventListener('resize', handleResize);
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  return (
    <ResizeListenerContext.Provider value={{ size }}>
      {children}
    </ResizeListenerContext.Provider>
  );
};

export default ResizeListenerProvider;
export const useResizeListenerContext = () => React.useContext(
  ResizeListenerContext,
);

ResizeListenerProvider.propTypes = {
  children: propTypes.node.isRequired,
};
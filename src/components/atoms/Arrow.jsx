import React from 'react';
import { RiArrowDropLeftLine, RiArrowDropRightLine } from 'react-icons/ri';
import propTypes from 'prop-types';

const Arrow = ({ onClick, direction }) => (
  <div
    className={`bg-light-100 rounded-full text-accent-100 h-14 w-14 text-8xl absolute top-1/2 z-10 hidden lg:flex lg:items-center lg:justify-center ${direction === 'left' ? 'left-6' : 'right-6'}`}
  >
    {
        direction === 'left'
          ? (
            <RiArrowDropLeftLine
              onClick={onClick}
              className='cursor-pointer'
            />
          )
          : (
            <RiArrowDropRightLine
              onClick={onClick}
              className='cursor-pointer'
            />
          )
      }
  </div>
);

Arrow.propTypes = {
  onClick: propTypes.func,
  direction: propTypes.oneOf([ 'left', 'right' ]).isRequired,
};

Arrow.defaultProps = {
  onClick: () => {},
};

export default Arrow;
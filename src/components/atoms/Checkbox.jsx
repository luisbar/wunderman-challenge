import React from 'react';
import { useActor, useMachine } from '@xstate/react';
import { FormattedMessage } from 'react-intl';
import propTypes from 'prop-types';

const Checkbox = ({ label, data, className, machine, test }) => {
  const actor = test ? useMachine(machine) : useActor(machine);
  const send = [ ...actor ][1];

  const onClick = () => {
    send({
      data,
      type: 'TOGGLE',
    });
  };

  return (
    <button
      type='button'
      data-testid='checkbox-container'
      className={`flex flex-col items-center cursor-pointer ${className}`}
      onClick={onClick}
    >
      <span className='dark:text-accent-300'>
        <FormattedMessage id={label} />
      </span>
      <input
        type='checkbox'
        className='hidden'
      />
      <div
        data-testid='checkbox-child-3'
        className='w-8 h-4 rounded-md duration-500 bg-dark-300 dark:bg-accent-100'
      >
        <div
          data-testid='checkbox-child-3.1'
          className='bg-light-100 w-4 h-4 rounded-full duration-500 dark:transform dark:translate-x-full'
        />
      </div>
    </button>
  );
};

Checkbox.propTypes = {
  label: propTypes.string.isRequired,
  data: propTypes.object.isRequired,
  className: propTypes.string,
  machine: propTypes.object.isRequired,
  test: propTypes.bool,
};

Checkbox.defaultProps = {
  className: '',
  test: false,
};

export default Checkbox;

import React from 'react';
import Main from '@templates/Main';
import Carousel from '@molecules/Carousel';
import Articles from '@organisms/Articles';
import { useStoreContext } from '@root/stateMachines/Store';
import { useActor } from '@xstate/react';
import ContactForm from '@organisms/ConctactForm';

const Home = () => {
  const { homeService } = useStoreContext();
  const [ homeState ] = useActor(homeService);

  return (
    <Main>
      <section
        className='h-full'
      >
        <Carousel
          button={{
            machine: homeState.children.buyButton,
            text: {
              label: 'home.txt1',
              className: 'text-light-100 text-3xl',
            },
            data: {
              type: 'NOTHING',
              data: {},
            },
          }}
        />
        <Articles />
        <ContactForm />
      </section>
    </Main>
  );
};

export default Home;

import Table from '@organisms/Table';
import Text from '@atoms/Text';
import Main from '@templates/Main';
import React from 'react';
import faker from 'faker';
import Paginator from '@molecules/Paginator';
import { useStoreContext } from '@root/stateMachines/Store';

const Components = () => {
  const { homeService } = useStoreContext();

  return (
    <Main>
      <div
        className='grid row-start-2 row-end-3 col-start-1 col-end-11 grid-rows-table-body-19 grid-cols-10 overflow-scroll'
      >
        <Text
          as='h1'
          label='components.txt1'
          className='row-start-1 col-start-5 justify-self-center self-center'
        />
        <Text
          as='h2'
          label='components.txt2'
          className='row-start-2 col-start-5 justify-self-center self-center'
        />
        <Text
          as='h3'
          label='components.txt3'
          className='row-start-3 col-start-5 justify-self-center self-center'
        />
        <Text
          as='h4'
          label='components.txt4'
          className='row-start-1 col-start-6 justify-self-center self-center'
        />
        <Text
          as='span'
          label='components.txt5'
          className='row-start-2 col-start-6 justify-self-center self-center'
        />
        <Text
          as='md'
          label='components.txt6'
          className='row-start-3 col-start-6 justify-self-center self-center'
        />
        <Table
          className='row-start-5 row-end-17 col-start-1 col-end-11 bg-dark-400'
          columns={[
            {
              key: 'firstName',
              value: 'components.txt7',
              type: 'text',
            },
            {
              key: 'lastName',
              value: 'components.txt8',
              type: 'text',
            },
            {
              key: 'email',
              value: 'components.txt9',
              type: 'text',
            },
            {
              key: 'phone',
              value: 'components.txt10',
              type: 'text',
            },
            {
              key: 'actions',
              value: 'components.txt11',
              type: 'actions',
              actions: [ { icon: 'BsFillEyeFill', onClickItem: (data) => { alert(data.firstName); } } ],
            },
          ]}
          rows={Array(10).fill(1).map(() => ({
            firstName: faker.name.firstName(),
            lastName: faker.name.lastName(),
            email: faker.internet.email(),
            phone: faker.phone.phoneNumber(),
          }))}
          headerActions={[ { icon: 'BsPlusSquareFill', onClickItem: () => { alert('Your header action!'); } } ]}
        />
        <Paginator
          className='row-start-17 col-start-9 col-end-11 justify-self-end self-center'
          machine={homeService.children.get('paginator')}
        />
      </div>
    </Main>
  );
};

export default Components;
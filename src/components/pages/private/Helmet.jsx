import Main from '@templates/Main';
import React from 'react';
import { useCurrentRoute } from 'react-navi';

const Helmet = () => {
  const { data } = useCurrentRoute();

  return (
    <Main>
      {data.name}
    </Main>
  );
};

export default Helmet;

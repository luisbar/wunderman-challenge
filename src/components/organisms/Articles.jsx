import React from 'react';
import Text from '@atoms/Text';
import Cards from '@organisms/Cards';
import faker from 'faker';

const Articles = () => (
  <div
    className='h-auto'
  >
    <Text
      className='text-center lg:ml-6 lg:text-left mb-6'
      as='h3'
      label='articles.txt1'
    />
    <Cards
      className='h-5/6'
      cards={Array(8).fill(1).map((item, index) => ({
        id: index,
        image: `https://source.unsplash.com/512x512/?${faker.address.cityName()}`,
        title: 'articles.txt2',
        body: 'articles.txt3',
      }))}
    />
  </div>
);

export default Articles;
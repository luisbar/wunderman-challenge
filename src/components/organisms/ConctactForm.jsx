import React, { useState } from 'react';
import Text from '@atoms/Text';
import Input from '@atoms/Input';
import Dropdown from '@atoms/Dropdown';
import Checkbox from '@atoms/Checkbox';
import Button from '@atoms/Button';
import { useStoreContext } from '@stateMachines/Store';

const locationsByDepartment = {
  Artigas: [
    {
      key: 'contact.form.txt10',
      value: 'contact.form.txt10',
    },
    {
      key: 'contact.form.txt14',
      value: 'contact.form.txt14',
    },
  ],
  Canelones: [
    {
      key: 'contact.form.txt11',
      value: 'contact.form.txt11',
    },
    {
      key: 'contact.form.txt15',
      value: 'contact.form.txt15',
    },
  ],
  Montevideo: [
    {
      key: 'contact.form.txt12',
      value: 'contact.form.txt12',
    },
  ],
  Salto: [
    {
      key: 'contact.form.txt13',
      value: 'contact.form.txt13',
    },
    {
      key: 'contact.form.txt16',
      value: 'contact.form.txt16',
    },
    {
      key: 'contact.form.txt17',
      value: 'contact.form.txt17',
    },
  ],
};

const ContactForm = () => {
  const { homeService } = useStoreContext();
  const [ data, setData ] = useState({
    firstname: undefined,
    lastname: undefined,
    email: undefined,
    department: undefined,
    location: undefined,
    ci: undefined,
  });
  const [ locations, setLocations ] = useState([]);

  const onChange = (event) => {
    setData((currentData) => ({
      ...currentData,
      [event.target.id]: event.target.value,
    }));
  };

  const onChangeDepartment = ({ target }) => {
    setData((currentData) => ({
      ...currentData,
      department: target.value,
    }));
    setLocations(locationsByDepartment[target.value]);
  };

  const onChangeLocation = ({ target }) => {
    setData((currentData) => ({
      ...currentData,
      location: target.value,
    }));
  };

  return (
    <div
      className='flex flex-col mt-36'
    >
      <Text
        className='text-center mb-6'
        as='h3'
        label='contact.form.txt1'
      />
      <div
        className=' bg-dark-100 w-full mb-6'
        style={{ height: 1 }}
      />
      <div
        className='flex flex-col m-6 lg:m-0 lg:grid lg:grid-rows-5 lg:grid-cols-10 lg:gap-3'
      >
        <Input
          machine={homeService.children.get('firstnameInput')}
          className='lg:row-start-1 lg:row-span-1 lg:col-start-2 lg:col-span-4'
          text={{
            label: 'contact.form.txt2',
          }}
          input={{
            onChange,
            type: 'text',
            id: 'firstname',
            placeholder: 'Roberto',
          }}
        />
        <Input
          machine={homeService.children.get('lastnameInput')}
          className='lg:row-start-1 lg:row-span-1 lg:col-start-6 lg:col-span-4'
          text={{
            label: 'contact.form.txt3',
          }}
          input={{
            onChange,
            type: 'text',
            id: 'lastname',
            placeholder: 'Gomez',
          }}
        />
        <Input
          machine={homeService.children.get('emailInput')}
          className='lg:row-start-2 lg:row-span-1 lg:col-start-2 lg:col-span-8'
          text={{
            label: 'contact.form.txt4',
          }}
          input={{
            onChange,
            type: 'email',
            id: 'email',
            placeholder: 'mail@mail.com',
          }}
        />
        <Dropdown
          machine={homeService.children.get('departmentInput')}
          className='lg:row-start-3 lg:row-span-1 lg:col-start-2 lg:col-span-4'
          text={{
            label: 'contact.form.txt5',
          }}
          select={{
            placeholder: true,
            onChange: onChangeDepartment,
            options: [
              {
                key: 'contact.form.txt10',
                value: 'contact.form.txt10',
              },
              {
                key: 'contact.form.txt11',
                value: 'contact.form.txt11',
              },
              {
                key: 'contact.form.txt12',
                value: 'contact.form.txt12',
              },
              {
                key: 'contact.form.txt13',
                value: 'contact.form.txt13',
              },
            ],
          }}
        />
        <Dropdown
          machine={homeService.children.get('locationInput')}
          className='lg:row-start-3 lg:row-span-1 lg:col-start-6 lg:col-span-4'
          text={{
            label: 'contact.form.txt6',
          }}
          select={{
            placeholder: true,
            onChange: onChangeLocation,
            options: locations,
          }}
        />
        <Input
          machine={homeService.children.get('ciInput')}
          className='lg:row-start-4 lg:row-span-1 lg:col-start-2 lg:col-span-4'
          text={{
            label: 'contact.form.txt7',
          }}
          input={{
            onChange,
            type: 'text',
            id: 'ci',
            placeholder: '63108823',
          }}
        />
        <Checkbox
          className='mt-5 self-start lg:row-start-4 lg:row-span-1 lg:col-start-6 lg:col-span-1'
          label='contact.form.txt8'
          data={{
            type: 'NOTHING',
          }}
          machine={homeService.children.get('agreeCheckbox')}
        />
        <Button
          className='h-12 w-40 self-center justify-self-center mt-6 lg:row-start-5 lg:row-span-1 lg:col-start-5 lg:col-span-2'
          machine={homeService.children.get('sendButton')}
          text={{
            label: 'contact.form.txt9',
            className: 'text-light-100',
          }}
          data={{
            type: 'SEND',
            data,
          }}
        />
      </div>
    </div>
  );
};

export default ContactForm;
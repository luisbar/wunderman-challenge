import React from 'react';
import Card from '@molecules/Card';
import propTypes from 'prop-types';

const Cards = ({ cards, className }) => (
  <div
    className={`grid grid-rows-splitted-into-8 grid-cols-1 gap-3 mx-6 lg:mx-0 lg:grid-rows-splitted-into-2 lg:grid-cols-splitted-into-4 lg:justify-center ${className}`}
  >
    {
      cards.map((card, index) => {
        const top = index % 2 !== 0 ? 'lg:top-5' : '';
        const rowStart = index > 3 ? 'lg:row-start-2' : 'lg:row-start-1';
        const colStart = index > 3 ? 'lg:col-start-' + (index - 4 + 1) : 'lg:col-start-' + (index + 1);
        return (
          <Card
            className={`relative row-start-${index + 1} row-span-1 col-start-1 col-span-1 ${top} ${rowStart} ${colStart}`}
            key={card.id}
            title={card.title}
            body={card.body}
            image={card.image}
          />
        );
      })
    }
  </div>
);

Cards.propTypes = {
  cards: propTypes.arrayOf(propTypes.objectOf(Card)).isRequired,
  className: propTypes.string,
};

Cards.defaultProps = {
  className: '',
};

export default Cards;
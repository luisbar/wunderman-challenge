import { createMachine, sendParent, send, actions } from 'xstate';
import { assign } from '@xstate/immer';
import buttonMachine from '@stateMachines/atoms/button';

const CHILDREN = [ 'previousButton', 'nextButton' ];

export const machineDefinition = {
  id: 'paginator',
  initial: 'enabled',
  context: {
    pageNumber: undefined,
    pageSize: undefined,
    total: undefined,
  },
  invoke: [
    {
      id: 'previousButton',
      src: buttonMachine,
    },
    {
      id: 'nextButton',
      src: buttonMachine,
    },
  ],
  states: {
    enabled: {
      entry: [ 'toggleButtons' ],
      on: {
        UPDATE_PAGINATION: {
          actions: [ 'updatePagination' ],
          target: 'enabled',
        },
        PREVIOUS: {
          actions: [ 'sendParentPreviousPageNumber' ],
          target: 'enabled',
        },
        NEXT: {
          actions: [ 'sendParentNextPageNumber' ],
          target: 'enabled',
        },
        DISABLE: {
          target: 'disabled',
        },
      },
    },
    disabled: {
      entry: [ 'disableAll' ],
      on: {
        ENABLE: {
          target: 'enabled',
        },
      },
      exit: [ 'enableAll' ],
    },
  },
};

const machineOptions = {
  actions: {
    toggleButtons: actions.pure(() => [
      actions.choose(
        [
          {
            cond: 'thereIsNotPreviousPage',
            actions: [ 'disablePreviousButton' ],
          },
          {
            cond: 'thereIsPreviousPage',
            actions: [ 'enablePreviousButton' ],
          },
        ],
      ),
      actions.choose(
        [
          {
            cond: 'thereIsNotNextPage',
            actions: [ 'disableNextButton' ],
          },
          {
            cond: 'thereIsNextPage',
            actions: [ 'enableNextButton' ],
          },
        ],
      ),
    ]),
    updatePagination: assign((
      context, { data: { pagination: { pageNumber, pageSize, total } } },
    ) => {
      context.pageNumber = pageNumber;
      context.pageSize = pageSize;
      context.total = total;
    }),
    sendParentPreviousPageNumber: sendParent(({ pageNumber }) => ({ type: 'PROCESS', data: { pageNumber: pageNumber - 1 } })),
    sendParentNextPageNumber: sendParent(({ pageNumber }) => ({ type: 'PROCESS', data: { pageNumber: pageNumber + 1 } })),
    disableAll: actions.pure(() => CHILDREN.map((child) => send({ type: 'DISABLE' }, { to: child }))),
    disablePreviousButton: send({ type: 'DISABLE' }, { to: 'previousButton' }),
    disableNextButton: send({ type: 'DISABLE' }, { to: 'nextButton' }),
    enableAll: actions.pure(() => CHILDREN.map((child) => send({ type: 'ENABLE' }, { to: child }))),
    enablePreviousButton: send({ type: 'ENABLE' }, { to: 'previousButton' }),
    enableNextButton: send({ type: 'ENABLE' }, { to: 'nextButton' }),
  },
  guards: {
    thereIsNotPreviousPage: (context) => !context.total || context.pageNumber <= 0,
    thereIsPreviousPage: (context) => context.total > 0 || context.pageNumber > 0,
    thereIsNotNextPage: (context) => !context.total
    || context.pageNumber >= Math.ceil(context.total / context.pageSize) - 1,
    thereIsNextPage: (context) => context.total > 0
    || (context.pageNumber < Math.ceil(context.total / context.pageSize) - 1),
  },
};

export default createMachine(machineDefinition, machineOptions);
import { createMachine } from 'xstate';

export const machineDefinition = {
  id: 'globalSpinner',
  initial: 'disabled',
  states: {
    disabled: {
      on: {
        ENABLE: 'enabled',
      },
    },
    enabled: {
      on: {
        DISABLE: 'disabled',
      },
    },
  },
};

export default createMachine(machineDefinition);
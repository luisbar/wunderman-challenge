import { createMachine, send, actions } from 'xstate';
import buttonMachine from '@stateMachines/atoms/button';
import dropdownMachine from '@stateMachines/atoms/dropdown';
import inputMachine from '@stateMachines/atoms/input';
import checkboxMachine from '@stateMachines/atoms/checkbox';
import schemaValidator from '@utils/schemaValidator';
import { assign } from '@xstate/immer';

const INPUTS_CHILDREN = [ 'firstnameInput', 'lastnameInput', 'emailInput', 'departmentInput', 'locationInput', 'ciInput', 'agreeCheckbox' ];
const CHILDREN_TO_DISABLED = [ ...INPUTS_CHILDREN, 'sendButton' ];

export const machineDefinition = {
  id: 'home',
  initial: 'idle',
  context: {
    features: [],
  },
  invoke: [
    {
      id: 'buyButton',
      src: buttonMachine,
    },
    {
      id: 'languagesDropdown',
      src: dropdownMachine,
    },
    {
      id: 'firstnameInput',
      src: inputMachine,
    },
    {
      id: 'lastnameInput',
      src: inputMachine,
    },
    {
      id: 'emailInput',
      src: inputMachine,
    },
    {
      id: 'departmentInput',
      src: dropdownMachine,
    },
    {
      id: 'locationInput',
      src: dropdownMachine,
    },
    {
      id: 'ciInput',
      src: inputMachine,
    },
    {
      id: 'agreeCheckbox',
      src: checkboxMachine,
    },
    {
      id: 'sendButton',
      src: buttonMachine,
    },
  ],
  states: {
    idle: {
      on: {
        NOTHING: [
          {
            target: 'idle',
          },
        ],
        SEND: [
          {
            cond: 'dataIsValid',
            actions: [ 'cleanErrorOnInputs' ],
            target: 'processing',
          },
          {
            actions: [ 'cleanErrorOnInputs', 'showErrorOnInputs' ],
            target: 'idle',
          },
        ],
      },
    },
    processing: {
      entry: [ 'disableAll' ],
      invoke: [
        {
          id: 'sendData',
          src: 'sendData',
          onDone: {
            target: 'finished',
          },
          onError: {
            actions: [ 'saveError' ],
            target: 'failed',
          },
        },
      ],
      exit: [ 'enableAll' ],
    },
    finished: {
      after: {
        1: {
          target: 'idle',
        },
      },
    },
    failed: {
      after: {
        1: {
          target: 'idle',
        },
      },
    },
  },
};

const machineOptions = {
  actions: {
    disableAll: actions.pure(() => CHILDREN_TO_DISABLED.map((child) => send({ type: 'DISABLE' }, { to: child, delay: 1 }))),
    enableAll: actions.pure(() => CHILDREN_TO_DISABLED.map((child) => send({ type: 'ENABLE' }, { to: child }))),
    saveError: assign((context, event) => { context.error = event.data; }),
    cleanErrorOnInputs: actions.pure(() => INPUTS_CHILDREN.map((input) => send({ type: 'CLEAN_ERROR' }, { to: input }))),
    showErrorOnInputs: actions
    .pure((context, event) => schemaValidator
      .validateContactData(event.data)
      .map((errorMessage) => {
        const targetInput = INPUTS_CHILDREN.filter((input) => errorMessage.includes(input.replace('Input', '')))[0];
        return send(
          { type: 'SHOW_ERROR', data: { error: errorMessage } },
          { to: targetInput, delay: 1 },
        );
      })),
  },
  guards: {
    dataIsValid: (context, event) => !schemaValidator.validateContactData(event.data),
  },
  services: {
    sendData: (context, event) => new Promise((resolve, reject) => {
      setTimeout(() => {
        if (Math.random() > 0.5) {
          resolve();
        } else {
          reject();
        }
      }, 4000);
    }),
  },
};

export default createMachine(machineDefinition, machineOptions);
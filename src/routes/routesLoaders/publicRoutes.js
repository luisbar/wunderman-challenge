import { route } from 'navi';
import publicPages from '@routes/config/publicPages.json';
import upperCamelCaseToLowerCamelCase from '@utils/upperCamelCaseToLowerCamelCase';

export default async () => {
  const routes = {};

  const getConfiguration = async (publicPage) => {
    try {
      return await import(
        `@pages/public/config/${upperCamelCaseToLowerCamelCase(
          publicPage.name,
        )}`
      );
    } catch (error) {
      return {
        default: {},
      };
    }
  };

  await Promise.all(
    publicPages.map((publicPage) => new Promise(async (resolve, reject) => {
      try {
        const configuration = await getConfiguration(publicPage);
        const path = configuration.default.path || publicPage.path;
        routes[path] = route(
          {
            title: publicPage.name,
            getView: () => import(
              /* webpackChunkName: 'public' */ `@pages/public/${publicPage.name}.${
                publicPage.extension
              }`
            ),
            ...configuration.default,
          },
        );
        resolve();
      } catch (error) {
        reject(error);
      }
    })),
  );

  return { ...routes };
};

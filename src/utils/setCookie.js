export default (
  key,
  value,
) => {
  document.cookie = `${key}=${value}`;
};

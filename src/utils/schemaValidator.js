import joi from 'joi';

const getErrorMessages = ({ schema, data }) => {
  const { error } = schema.validate(data, { abortEarly: false });

  if (!error) return undefined;

  return error
    .details
    .map((detail) => `error.${detail.path[0]}.${detail.type}`);
};

const validateSessionData = (data = {}) => {
  const schema = joi.object({
    username: joi.string()
      .required()
      .email({ minDomainSegments: 2, tlds: { allow: [ 'com', 'net', 'io' ] } }),
    password: joi.string()
      .required()
      .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')),
  });

  return getErrorMessages({ schema, data });
};

const validateContactData = (data = {}) => {
  const schema = joi.object({
    firstname: joi.string()
      .required()
      .pattern(new RegExp('^[a-zA-Z]{3,30}$')),
    lastname: joi.string()
      .required()
      .pattern(new RegExp('^[a-zA-Z]{3,30}$')),
    email: joi.string()
      .required()
      .email({ minDomainSegments: 2, tlds: { allow: [ 'com', 'net', 'io' ] } }),
    department: joi.string()
      .required()
      .pattern(new RegExp('^[a-zA-Z]{3,30}$')),
    location: joi.string()
      .required()
      .pattern(new RegExp('^[a-zA-Z]{3,30}$')),
    ci: joi.string()
      .required()
      .custom((ci, helpers) => {
        //Inicializo los coefcientes en el orden correcto
        var arrCoefs = new Array(2, 9, 8, 7, 6, 3, 4, 1);
        var suma = 0;
        //Para el caso en el que la CI tiene menos de 8 digitos
        //calculo cuantos coeficientes no voy a usar
        var difCoef = parseInt(arrCoefs.length - ci.length);
        //recorro cada digito empezando por el de más a la derecha
        //o sea, el digito verificador, el que tiene indice mayor en el array
        for (var i = ci.length - 1; i > -1; i--) {
            //Obtengo el digito correspondiente de la ci recibida
            var dig = ci.substring(i, i + 1);
            //Lo tenía como caracter, lo transformo a int para poder operar
            var digInt = parseInt(dig);
            //Obtengo el coeficiente correspondiente al ésta posición del digito
            var coef = arrCoefs[i + difCoef];
            //Multiplico dígito por coeficiente y lo acumulo a la suma total
            suma = suma + digInt * coef;
        }
        var result = false;
        // si la suma es múltiplo de 10 es una ci válida
        if ((suma % 10) === 0) {
            result = true;
        }

        if (!result) return helpers.error('custom.invalid');

        return result;
      })
  });

  return getErrorMessages({ schema, data });
};

export default {
  validateSessionData,
  validateContactData
};
const colors = require('tailwindcss/colors');
const defaultTheme = require('tailwindcss/defaultTheme');

const generateTemplate = ({ number, prefix, measure }) => {
  let template = {};
  Array(number).fill(1).forEach((item, index) => template[`${prefix}-${index + 1}`] = `repeat(${index + 1}, ${measure})`);
  return template;
}

const generateStartAndEnd = ({ number }) => {
  let startAndEnd = {};
  Array(number).fill(1).forEach((item, index) => startAndEnd[String(index)] = String(index));
  return startAndEnd;
}

module.exports = {
  purge: ['./src/**/*.{js,jsx,mdx}', './public/index.html'],
  darkMode: 'class',
  theme: {
    colors: {
      ...colors,
      'primary': '#818CF8',
      'primary-light': '#A5B4FB',
      'primary-dark': '#6466f1',
      'accent-100': '#4da6e4',
      'accent-200': '#81E4DA',
      'accent-300': '#47E5BC',
      'light-100': '#FFFFFF',
      'dark-100': '#343434',
      'dark-200': '#3F3F46',
      'dark-300': '#CACACA',
      'dark-400': '#FAFAFA',
      'error': '#F8A5A5',
      'success': '#86EFAC',
    },
    extend: {
      spacing: defaultTheme.spacing,
      borderRadius: defaultTheme.borderRadius,
      gridRowStart: {
        ...generateStartAndEnd({ number: 50 }),
      },
      gridRowEnd: {
        ...generateStartAndEnd({ number: 50 }),
      },
      gridColumnStart: {
        ...generateStartAndEnd({ number: 50 }),
      },
      gridColumnEnd: {
        ...generateStartAndEnd({ number: 50 }),
      },
      gridTemplateRows: {
        'side-menu': '25% repeat(10, 5%)',
        'main': '10% 80% 10%',
        'table': '0.5fr 0.5fr 6fr',
        ...generateTemplate({ number: 50, prefix: 'table-body', measure: '5rem' }),
        'splitted-into-2': `repeat(2, 50%)`,
        'splitted-into-8': `repeat(8, 12.5%)`,
        'splitted-into-10': `repeat(10, 10%)`,
        'splitted-into-20': `repeat(20, 5%)`,
      },
      gridTemplateColumns: {
        'splitted-into-4': `repeat(4, 24%)`,
        'splitted-into-10': `repeat(10, 10%)`,
        'splitted-into-20': `repeat(20, 5%)`,
      },
    },
  },
  variants: {
    extend: {
      transform: ['dark'],
      translate: ['dark'],
      cursor: ['disabled'],
    },
  },
  plugins: [
    require('@tailwindcss/forms'),
  ],
}
